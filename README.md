* Spring container? 
    - ApplicationContext 
        - ConfigurableApplicationContext
        - ApplicationContextAware
        - other aware interfaces
* Bean instantiation
    - @Bean in @Configuration
    - @Service
    - @Component
    - @Repository
    - @Controller
* Bean scopes
    - @Scope
        - default scope
    - @SessionScope
    - @RequestScope
    - @ApplicationScope 
        - ConfigurableBeanFactory
        - WebApplicationContext
        - BeanDefinition
* Bean lifecycle
    - @PostConstruct
    - @PreDestroy
    
* Autowiring
    - interfejsy a implementacje
    - lista obiektów
* Configuration
    @Value
    
    @Bean
    @Qualifier
    @Component
        @Named
        @ManagedBean   
    @ComponentScan
        @Filter
    @EnableAutoConfiguration
    @ImportResource
    @Profile
        -> https://www.baeldung.com/spring-profiles
        -> https://stackoverflow.com/questions/40060989/how-to-use-spring-boot-profiles
* events
    Built-in events
        ContextRefreshedEvent
        ContextStartedEvent
        ContextStoppedEvent
        ContextClosedEvent
        RequestHandledEvent
        ServletRequestHandledEvent
     @EventListener   
     @Async
     @Order   
    
    Instantiating Beans
        factory
        
    Naming Beans
    Bean Scopes
    Dependency Injection
        constructor
        properties
    Autowiring mode
    Lazy initialization mode
    Initialization method
    Destruction method
3. Annotations
    @Bean
    


ApplicationContext
